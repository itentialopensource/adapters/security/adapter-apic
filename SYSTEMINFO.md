# APIC

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: APIC
Product Page: https://www.cisco.com/c/en/us/products/cloud-systems-management/application-policy-infrastructure-controller-apic/index.html

## Introduction
We classify APIC into the Security/SASE domain as APIC provides a Network Security solution. We also classify APIC into the Network Services domain since APIC helps manage and automate network policies and configurations.

"The APIC automates network provisioning and control that is based on the application requirements and policies"

## Why Integrate
The APIC adapter from Itential is used to integrate the Itential Automation Platform (IAP) with APIC to optimize performance and manage and operate a scalable multitenant Cisco ACI fabric. With this adapter you have the ability to perform operations such as:

- Create Tenant
- Delete Tenant
- Create Tenant Application Profile
- Create Tenant Application Profile EPG

## Additional Product Documentation
The [API documents for APIC](https://www.cisco.com/c/en/us/td/docs/switches/datacenter/aci/apic/sw/3-x/rest-api-config/b_Cisco_APIC_REST_API_Configuration_Guide_3x.html)